<nav class="navbar navbar-inverse">
  <div class="container-fluid">  

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>  

  
    <?php wp_nav_menu( array(
      'menu'              => 'menu-1',
  		'depth'             => 2,
  		'container'         => 'div',
  		'container_class'   => 'collapse navbar-collapse',
  		'container_id'      => 'main-navbar',
  		'menu_class'        => 'nav navbar-nav',
  		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
  		'walker'            => new wp_bootstrap_navwalker())
    ); ?>
  </div>
</nav>