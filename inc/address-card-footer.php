<div class="address-card">
	<h2 class="h4">Contact us</h2>
	<address itemscope="" itemtype="http://schema.org/LocalBusiness">
		<span class="card-map-phone" itemprop="telephone">Tel: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>
		<span class="card-map-phone" itemprop="telephone">Email: <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></span><br>
	</address>
</div>

