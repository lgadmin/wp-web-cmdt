<?php
	$cta_title = get_field('cta_title', 'option');
	$cta_subtitle = get_field('cta_subtitle', 'option');
	$cta_background_color = get_field('cta_background_color', 'option');
	$cta_button = get_field('cta_button', 'option');
	//Group - button_text, button_link
?>

<div class="site-cta block" style="background-color: <?php echo $cta_background_color; ?>;">
	<div class="container">
		<div class="copy">
			<?php if($cta_title): ?>
				<h3><?php echo $cta_title; ?></h3>
			<?php endif; ?>
			<?php if($cta_title): ?>
				<p><?php echo $cta_subtitle; ?></p>
			<?php endif; ?>
		</div>
		<div class="cta-button">
			<a href="<?php echo $cta_button['button_link']; ?>"><?php echo $cta_button['button_text']; ?></a>
		</div>
	</div>
</div>