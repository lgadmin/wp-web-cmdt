<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div><!-- #content -->

<?php if (! is_front_page()) : ?>
	<?php get_template_part('/template-parts/cta-area'); ?>
<?php endif; ?>




	<footer id="colophon" class="site-footer">
		<div class="bg-gray-base clearfix pb-lg pt-lg">

			<div class="footer-brand">
				<?php echo do_shortcode('[lg-site-logo]'); ?> 
				<div class="small"><?php echo get_field('footer_blurb', 'option'); ?></div>
			</div>

			<div class="blog-feed">
				<div class="east">
					<?php get_template_part("/inc/address-card-footer"); ?>
					<?php get_template_part("/inc/location-list"); ?>
				</div>
				<div class="west">
					<?php get_template_part("/inc/service-list"); ?>
				</div>
			</div>
			
			<div class="footer-utility">
				<h2 class="h4">Current Weather</h2>
				  <div id="plemx-root"></div> 
				  <a href="http://www.theweathernetwork.com">The Weather Network</a>
				  <script type="text/javascript"> 

				  var _plm = _plm || [];
				  _plm.push(['_btn', 78603]); 
				  _plm.push(['_loc','cabc0308']);
				  _plm.push(['location', document.location.host ]);
				   (function(d,e,i) {
				  if (d.getElementById(i)) return;
				  var px = d.createElement(e);
				  px.type = 'text/javascript';
				  px.async = true;
				  px.id = i;
				  px.src = ('https:' == d.location.protocol ? 'https:' : 'http:') + '//widget.twnmm.com/js/btn/pelm.js?orig=en_ca';
				  var s = d.getElementsByTagName('script')[0];

				  var py = d.createElement('link');
				  py.rel = 'stylesheet'
				  py.href = ('https:' == d.location.protocol ? 'https:' : 'http:') + '//widget.twnmm.com/styles/btn/styles.css'

				  s.parentNode.insertBefore(px, s);
				  s.parentNode.insertBefore(py, s);
				})(document, 'script', 'plmxbtn');</script>

			</div>
		</div>
		<div class="bg-gray-darker clearfix bottom-bar">
			<div class="site-info"><?php get_template_part("/inc/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/inc/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
