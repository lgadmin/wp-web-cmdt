<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>  
<!doctype html>

<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<header id="masthead" class="site-header">
		
		<div class="header-main">

			<div class="site-branding"> <?php echo do_shortcode('[lg-site-logo]'); ?> </div>
			
			<section class="site-description">
				<span class="h4 text-uppercase"><?php echo get_bloginfo( 'description', 'raw' ) ?></span>
			</section>

			
			<section class="site-btn">
				<a href="/stamped-concrete-quote/" class="btn btn-primary btn-lg text-uppercase">Free Consultation</a><br/><br/>
				Vancouver: <a href="tel:604-340-4509">604-340-4509</a><br/>
				Kamloops: <a href="tel:604-445-2740">604-445-2740</a>
			</section>
		</div>

		<?php  get_template_part("/inc/nav-main"); ?>

	</header>
