<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
			$result = query_posts(array( 
		        'post_type' => 'project',
		        'showposts' => -1 
		    )); 
		?>

		<div class="block container current-projects center">
			<!-- <h2 class="h2">Current Projects</h2> -->
			<ul class="list4grid">
				<?php foreach ($result as $key => $value): 
					$id = $value->ID;
					$image = get_the_post_thumbnail($id);
					$title = $value->post_title;
					$url = $value->guid;
					$status = get_field('project_completion', $id);
				?>

				<?php if(!$status): ?>
					<li>
						<div class="image">
							<?php echo $image; ?>
						</div>
						<div class="description">
							<?php if($title): ?>
								<h2 class="h5 no-icon text-center pt-sm pb-sm"><?php echo $title; ?></h2>
							<?php endif; ?>
							<a href="<?php echo $url; ?>" class="learn-more">Learn More</a>
						</div>
					</li>
				<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>

		<div class="block container current-projects center">
			<h2 class="h2">Completed Projects</h2>
			<ul class="list4grid">
				<?php foreach ($result as $key => $value): 
					$id = $value->ID;
					$image = get_the_post_thumbnail($id);
					$title = $value->post_title;
					$url = $value->guid;
					$status = get_field('project_completion', $id);
				?>

				<?php if($status): ?>
					<li>
						<div class="image">
							<?php echo $image; ?>
						</div>
						<div class="description">
							<?php if($title): ?>
								<h2><?php echo $title; ?></h2>
							<?php endif; ?>
							<a href="<?php echo $url; ?>" class="learn-more">Learn More</a>
						</div>
					</li>
				<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>

		</main>
	</div>
</div>
<?php get_footer();
