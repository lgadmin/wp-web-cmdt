<?php
/**
 * Default Page Template. Fixed width, no sidebar
 */

get_header(); ?>
<div id="content" class="page-home site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="home-slider">
				<?php masterslider(1); ?>
				<div class="layer">
					<div class="text-layer">
					</div> 
					<div class="form-layer">

						<?php echo do_shortcode('[gravityform id=1 title=true description=true ajax=true tabindex=49]'); ?>
					</div>
				</div>
			</div>
			<!-- end Home Slider -->

			<!-- Intro Section -->
			<?php
				$intro_title = get_field('intro_title');
				$intro_background_image = get_field('intro_background_image');
				$intro_sub_title = get_field('intro_sub_title');
			?>
			<div class="block center" style="background-image: url('<?php echo $intro_background_image; ?>');">
				<div class="container">
					<?php if($intro_title): ?>
						<h1 class="h2"><?php echo $intro_title; ?></h1>
					<?php endif; ?>

					<div class="mb-lg pb-lg">
						<?php if($intro_sub_title): ?>
						<h2 class="h5 no-icon">
							<?php echo $intro_sub_title; ?>
						</h2>
						<?php endif; ?>

						<?php
							$args = array(
					            'showposts'	=> -1,
					            'post_type'		=> 'location',
					        );
					        $result = new WP_Query( $args );
					        $title_list = [];

					        // Loop
					        if ( $result->have_posts() ) :
					            while( $result->have_posts() ) : $result->the_post();
					        		array_push($title_list, get_the_title());
					            endwhile;
					        endif; // End Loop

					        wp_reset_query();
						?>

						<p><?php echo join(", ", $title_list) . ' and more.'; ?></p>
					</div>

					<?php
					if( have_rows('intro_box') ):
						?>
						<ul class="home-introgrid">
						<?php
					    while ( have_rows('intro_box') ) : the_row();
					        $box_type = get_sub_field('box_type');

					        switch ($box_type) {
							    case "page":
							    	$thumbnail = get_sub_field('thumbnail');
							    	$headline = get_sub_field('headline');
							        ?>
									<li class="box-page">
										<div class="img-cont">
											<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
										</div>
										<div class="headline">
											<?php echo $headline; ?>
										</div>
									</li>
							        <?php
							        break;
							    case "video":
							    	$video = get_sub_field('video');
							        echo $video;
							        break;
							}
					    endwhile;
					    ?>
					    </ul>
					    <?php
					else :
					    // no rows found
					endif;
					?>
				</div>
			</div>
			<!-- end Intro Section -->

			<!-- Branch Service Section -->
			<?php 
				$branch_services_title = get_field('branch_services_title');
				$branch_services_description = get_field('branch_services_description');
				$branch_residential = get_field('branch_residential');
				$branch_commercial = get_field('branch_commercial');
				$branch_other = get_field('branch_other');
			?>

			<div class="block container center">
				<?php if($branch_services_title): ?>
					<h2 class="h2"><?php echo $branch_services_title; ?></h2>
				<?php endif; ?>
				<?php echo $branch_services_description; ?>


				<?php if($branch_residential): 
					$title = $branch_residential['title'];
					$image = $branch_residential['image'];
					$sub_title = $branch_residential['sub_title'];
					$services = $branch_residential['services'];
				?>

				<div class="split-content">
					<div class="half-image">
						<?php if($image): ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						<?php endif; ?>
					</div>
					<div class="half-copy">
						<?php if($title): ?>
							<h3 class="service-title"><?php echo $title; ?></h3>
						<?php endif; ?>
						<?php if($sub_title): ?>
							<h4 class="service-subtitle"><?php echo $sub_title; ?></h4>
						<?php endif; ?>

						<?php if(!empty($services)): ?>
							<ul class="services-list">
								<?php foreach($services as $item): ?>
									<li><?php echo $item['title']; ?></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>

				<?php endif; ?>


				<?php if($branch_commercial): 
					$title = $branch_commercial['title'];
					$image = $branch_commercial['image'];
					$services = $branch_commercial['services'];
				?>

				<div class="split-content reverse">
					<div class="half-image">
						<?php if($image): ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						<?php endif; ?>
					</div>
					<div class="half-copy">
						<?php if($title): ?>
							<h3 class="service-title"><?php echo $title; ?></h3>
						<?php endif; ?>

						<?php if(!empty($services)): ?>
							<ul class="services-list-1">
								<?php foreach($services as $item): ?>
									<li><?php echo $item['title']; ?></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>

				<?php endif; ?>


				<?php if($branch_other): 
					$title = $branch_other['title'];
					$image = $branch_other['image'];
					$services = $branch_other['services'];
					$bottom_blurb = $branch_other['bottom_blurb'];
				?>

				<div class="split-content reverse">
					<div class="half-image">
						<?php if($image): ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						<?php endif; ?>
					</div>
					<div class="half-copy">
						<?php if($title): ?>
							<h3 class="service-title"><?php echo $title; ?></h3>
						<?php endif; ?>

						<?php if(!empty($services)): ?>
							<ul class="services-list-1">
								<?php foreach($services as $item): ?>
									<li><?php echo $item['title']; ?></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>

						<?php if($bottom_blurb): ?>
							<?php echo $bottom_blurb; ?>
						<?php endif; ?>
					</div>
				</div>

				<?php endif; ?>
				
			</div>

			<!-- end Branch Service Section -->

			<!-- Gray Section -->
			<?php
			$gray_section_title = get_field('gray_section_title');
			$gray_section_description = get_field('gray_section_description');
			$gray_section_column_content = get_field('gray_section_column_content');
			?>

			<div class="block gray-section center" style="background-image: url('<?php echo $intro_background_image; ?>');">
				<div class="container">
					<?php if($gray_section_title): ?>
						<h2 class="h2"><?php echo $gray_section_title; ?></h2>
					<?php endif; ?>
					<?php echo $gray_section_description; ?>

					<div class="no-pb split-content">
						<div class="half-image">
							<?php if($gray_section_column_content['image']): ?>
								<img src="<?php echo $gray_section_column_content['image']['url']; ?>" alt="<?php echo $gray_section_column_content['image']['alt']; ?>">
							<?php endif; ?>
						</div>
						<div class="half-copy">
							<?php echo $gray_section_column_content['copy']; ?>
						</div>
					</div>
				</div>
			</div>
			<!-- end Gray Section -->

		</main>
	</div>
</div>
<?php get_footer();
