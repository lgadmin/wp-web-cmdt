<?php
/**
 * Contact Page
 */

get_header(); ?>

<div id="content" class="site-content">
	<div id="primary" class="content-area contact-page">
		<main id="main" class="site-main">
			<?php get_template_part('/inc/featured-slider'); ?>
			<div class="block container center">
				<div class="contact-info">
					<div class="contact-form">
						<?php echo do_shortcode(get_field('contact_form')); ?>
					</div>
				</div>
			</div>

		</main>
	</div>
</div>

<?php get_footer();
