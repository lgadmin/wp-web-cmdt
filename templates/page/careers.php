<?php
/**
 * Careers Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area career-page">
		<main id="main" class="site-main">

			<?php
				$intro_title = get_field('intro_title');
				$content = get_field('content');
				$cta = get_field('cta');
				$background_image = get_field('background_image');
			?>

			<div class="block intro-block center" style="background-image: url(<?php echo $background_image; ?>);">
				<div class="container">
					<h1 class="h2">
						<?php echo $intro_title; ?>
					</h1>
					<div class="split-content">
						<div class="half-image">
							
						</div>
						<div class="half-copy">
							<?php echo $content; ?>
							<a class="cta" href="<?php echo $cta['url']; ?>"><?php echo $cta['text']; ?></a>
						</div>
					</div>
				</div>
			</div>

		</main>
	</div>
</div>
<?php get_footer();
