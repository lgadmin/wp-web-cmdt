<?php
/**
 * About Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area concrete-stamps-page">
		<main id="main" class="site-main">

			<?php
				get_template_part('/inc/featured-slider');
			?>

			<!-- Intro Section -->
			<?php 
				$intro_title = get_field('intro_title');
				$intro_description = get_field('intro_copy');
				$title_logo = get_field('title_logo');
			?>

				<div class="block about-intro center">
					<div class="container">
						<?php if($intro_title): ?>
						<h1 class="h2">
							<?php echo $intro_title; ?>
						</h1>
						<?php endif; ?>

						<?php if($intro_description): ?>
							<?php echo $intro_description; ?>
						<?php endif; ?>
					</div>
				</div>
			<!-- end Intro Section -->

			<!-- Title Logo -->
				
			<!-- end Title Logo -->
				<div class="title-logo">
					<img src="<?php echo $title_logo['url']; ?>" alt="<?php echo $title_logo['alt']; ?>">
				</div>
			<!-- Stamps List -->
			<?php
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'stamp',
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	?>
			        	<div class="block container">
				        	<ul class="stamps list-unstyled">
				        	<?php
				            while( $result->have_posts() ) : $result->the_post();
				            	$title = get_the_title();
				            	$thumbnail = get_the_post_thumbnail();
				            	$icon = get_field('icon');
				        	?>
				        		<li>
				        			<div class="stamp-thumbnail">
				        				<?php echo $thumbnail; ?>
				        			</div>
				        			<div class="detail">
				        				<div class="icon"> <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>"> </div>
				        				<div class="header"> <h2><?php echo $title; ?></h2> </div>
				        			</div>
				        		</li>
							<?php
				            endwhile;
				            ?>
				            </ul>
			            </div>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
				?>
				<!-- end Stamp List -->

			<!-- CTA -->
				<?php // get_template_part('/template-parts/cta-area'); ?>
			<!-- end CTA -->

		</main>
	</div>
</div>
<?php get_footer();
