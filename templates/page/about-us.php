<?php
/**
 * About Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area about-page">
		<main id="main" class="site-main">

			<?php get_template_part('/inc/featured-slider'); ?>

			<!-- Intro Section -->
			<?php 
				$intro_title = get_field('intro_title');
				$intro_description = get_field('intro_description');
				$intro_background = get_field('intro_background');
			?>

			<div class="container center about-intro mb-lg pb-lg">
				<div class="container">
					<?php if($intro_title): ?>
					<h1 class="h2">
						<?php echo $intro_title; ?>
					</h1>
					<?php endif; ?>

					<?php if($intro_description): ?>
						<?php echo $intro_description; ?>
					<?php endif; ?>
				</div>
			</div>
			<!-- end Intro Section -->


			<!-- Section White -->
			<?php
				$section_white_icon = get_field('section_white_icon');
				$section_white_title = get_field('section_white_title');
				$section_white_content = get_field('section_white_content');
			?>
			<?php if ( is_array($section_white_icon) && $section_white_title && $section_white_title ): ?>

				<div class="section-white container center mb-lg pb-lg">
					<img src="<?php echo $section_white_icon['url']; ?>" alt="<?php echo $section_white_icon['alt']; ?>">
					<?php if($section_white_title): ?>
						<h2 class="h3"><?php echo $section_white_title; ?></h2>
					<?php endif; ?>
					<?php echo $section_white_content; ?>
				</div>
			<!-- end Section White -->
			<?php endif; ?>

			<!-- Section Gray -->
			<?php
				$section_gray_icon = get_field('section_gray_icon');
				$section_gray_title = get_field('section_gray_title');
				$section_gray_content = get_field('section_gray_content');
				$section_gray_background_image = get_field('section_gray_background_image');
			?>
			<?php if ( is_array($section_gray_icon) && $section_gray_title && $section_gray_title ): ?>
				<div class="section-gray center pb-lg" style="overflow:hidden; background: linear-gradient( rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5) ),url('<?php echo $section_gray_background_image; ?>')">
					<div class="container">
						<img src="<?php echo $section_gray_icon['url']; ?>" alt="<?php echo $section_gray_icon['alt']; ?>">
						<?php if($section_gray_title): ?>
							<h2 class="h3"><?php echo $section_gray_title; ?></h2>
						<?php endif; ?>
						<?php echo $section_gray_content; ?>
					</div>
				</div>
			<!-- end Section Gray -->
			<?php endif; ?>

		</main>
	</div>
</div>

<!-- Hide on testimonials page -->
<?php if($post->post_name != 'testimonials'): ?>
<div class="testimonial center pt-lg mt-lg">
	<div class="container">
		<?php echo do_shortcode(get_field('testimonial', 'option')); ?>
	</div>
</div>
<?php endif; ?>
 
<?php get_footer();
