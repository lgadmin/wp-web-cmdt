<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
		<!-- Intro -->
			<?php 
				$term = get_queried_object();
				$id = $term->term_id;
				$title = $term->name;
				$description = $term->description;
			?>
	
			<div class="block container center myintro">
				<?php // if($title): ?>
					<!-- <h2><?php // echo $title; ?></h2> -->
				<?php // endif; ?>
				<?php echo $description; ?>
			</div>
		<!-- end Intro -->




		<!-- Grid -->
			<div class="mt-lg container center">
				<?php if ( have_posts() ) : ?>
					<div class="loop-contents">
						<ul class="list4grid">
							<?php while ( have_posts() ) : the_post(); ?>
									
								<li>
									<div class="image">
										<?php the_post_thumbnail(); ?>
									</div>
									<div class="description">
										<h2><?php the_title(); ?></h2>
										<a href="<?php echo get_permalink(); ?>" class="learn-more">Learn More</a>
									</div>
								</li>
									
							<?php endwhile; ?>
						</ul>
					</div>
					
				<?php the_posts_navigation();
					else :
						get_template_part( 'template-parts/content', 'none' );
				endif; ?>
			</div>
		<!-- end Grid -->


		

		<!-- Benefits -->
			<?php $benefits = get_field('benefits', $term); ?>

			<div class="block container center mybenefits">
				<h2 class="h2"><?php echo $benefits['title']; ?></h2>
				<?php echo $benefits['content']; ?>
			</div>
		<!-- end Benefits -->

		</main>
	</div>
</div>
<?php get_footer();
