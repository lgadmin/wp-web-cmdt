<?php
/**
 * Single Project Page
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area single-service">
		<main id="main" class="site-main">

			<?php get_template_part('/inc/featured-slider');?>

			<!-- Intro Section -->
			<?php
				$intro_title = get_field('intro_title');
				$intro_description = get_field('intro_description');
			?>

				<div class="block container center">
					<?php if($intro_title): ?>
					<h1 class="h2">
						<?php echo $intro_title; ?>
					</h1>
					<?php endif; ?>
					<?php echo $intro_description; ?>
				</div>
			<!-- end Intro Section -->

			<!-- Bottom Slider -->
			<?php
				$slider = get_field('slider');
				$title = local_or_global(get_field('gallery_title'), get_field('gallery_header_default', 'option'));
				$background_image = local_or_global(get_field('gallery_background_image'), get_field('gallery_background_image_default','option'));
			?>

				<div class="block center" style="background-image: url('<?php echo $background_image; ?>');">
					<div class="container">
						<?php if($title): ?>
							<h2 class="h2 sr-only"><?php echo $title; ?></h2>
						<?php endif; ?>
						<div class="slider">
							<?php echo do_shortcode($slider); ?>
						</div>
					</div>
				</div>
			<!-- end Bottom Slider -->

		</main>
	</div>
</div>
<?php get_footer();
