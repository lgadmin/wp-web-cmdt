<?php
	$section1 = get_field('location_section1', 'option');
	$section2 = get_field('location_section2', 'option');
	$section3 = get_field('location_section3', 'option');
	$feature = get_field('location_feature', 'option');
?>

<?php
get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area single-location">
		<main id="main" class="site-main">

			<?php
				$feature = get_field('location_feature', 'option');
				$page_headline = $feature['headline'];
				$featured_slider = $feature['slider'];
			?>

			<div class="feature-banner">
				<?php if($featured_slider): ?>
					<?php echo do_shortcode($featured_slider); ?>
				<?php endif; ?>

				<?php if($page_headline): ?>
				<h2 class="headline">
					<?php echo $page_headline; ?>
				</h2>
				<?php endif; ?>
			</div>

			<!-- Section 1 -->
			<?php
				$section1 = get_field('location_section1', 'option');
			?>

			<div class="block container center section1">
				<h1 class="h2 grass-icon">
					<?php echo get_the_title() . ' ' . $section1['title']; ?>
				</h1>
				<?php echo $section1['content']; ?>
			</div>
			<!-- end Section 1 -->

			<!-- Section 2 -->
			<?php
				$section2 = get_field('location_section2', 'option');
			?>

			<div class="block section2 center" style="background-image:url('<?php echo $section2['background_image']; ?>');">
				<div class="container">
					<img src="<?php echo $section2['icon']['url']; ?>" alt="<?php echo $section2['icon']['alt']; ?>">
					<h2 class="h3"><?php echo $section2['title']; ?></h2>
					<?php echo $section2['content']; ?>
				</div>
			</div>
			<!-- end Section 2 -->

			<!-- Section 3 -->
			<?php
				$section3 = get_field('location_section3', 'option');
			?>

			<div class="section3 split-content reverse">
				<div class="half-image">
					<img src="<?php echo $section3['image']['url']; ?>" alt="<?php echo $section3['image']['alt']; ?>">
				</div>
				<div class="half-copy">
					<h2><?php echo $section3['title']; ?></h2>
					<?php echo $section3['content']; ?>
					<?php
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'location',
				        );
				        $result = new WP_Query( $args );

				        // Loop
				        if ( $result->have_posts() ) :
				        	?>
				        	<ul>
				        	<?php
				            while( $result->have_posts() ) : $result->the_post();
				        	?>
				        		<li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?> Landscaping Design</a></li>
							<?php
				            endwhile;
				            ?>
				            </ul>
				            <?php
				        endif; // End Loop

				        wp_reset_query();
					?>
				</div>
			</div>
			<!-- end Section 3 -->

		</main>
	</div>
</div>
<?php get_footer();
