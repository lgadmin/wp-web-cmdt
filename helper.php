<?php

/*
*	Para: Local, Global
*/

function local_or_global($local, $global){
	if($local)
		return $local;
	else
		return $global;
}


if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

?>